﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterFlow : MonoBehaviour {

    float x = 0.5f;
    float offsetX;
    public Renderer rend;
	void Start () {
        rend = GetComponent<Renderer>();
         offsetX= x*Time.deltaTime;
    }
	
	// Update is called once per frame
	void Update () {
        offsetX = x * Time.time;
        rend.material.mainTextureOffset = new Vector2(offsetX, 0f); ;
	}
}
