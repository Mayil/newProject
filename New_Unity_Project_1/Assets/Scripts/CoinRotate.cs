﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinRotate : MonoBehaviour {

    GameManager gameMan;
	void Start () {
        gameMan = GameManager.gm;
	}
	
	
	void Update () {
        transform.Rotate(new Vector3(0, 45, 0) * Time.deltaTime);
	}

    void OnTriggerEnter(Collider other)
    {
        
        gameObject.SetActive(false);
        gameMan.Upscore();

    }
}
